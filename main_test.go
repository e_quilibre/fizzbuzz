package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPingRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func TestFizzBuzzRoute(t *testing.T) {
	router := setupRouter()

	var contexts = []struct {
		QueryString string
		HTTPCode    int
		JSON        string
	}{
		{"", 200, `["1","2","fizz","4","buzz","fizz","7","8","fizz","buzz","11","fizz","13","14","fizzbuzz","16","17","fizz","19","buzz","fizz","22","23","fizz","buzz","26","fizz","28","29","fizzbuzz","31","32","fizz","34","buzz","fizz","37","38","fizz","buzz","41","fizz","43","44","fizzbuzz","46","47","fizz","49","buzz","fizz","52","53","fizz","buzz","56","fizz","58","59","fizzbuzz","61","62","fizz","64","buzz","fizz","67","68","fizz","buzz","71","fizz","73","74","fizzbuzz","76","77","fizz","79","buzz","fizz","82","83","fizz","buzz","86","fizz","88","89","fizzbuzz","91","92","fizz","94","buzz","fizz","97","98","fizz","buzz"]`},
		{"limit=15", 200, `["1","2","fizz","4","buzz","fizz","7","8","fizz","buzz","11","fizz","13","14","fizzbuzz"]`},
		{"limit=15&string1=bat&string2=man", 200, `["1","2","bat","4","man","bat","7","8","bat","man","11","bat","13","14","batman"]`},
		{"limit=30&int1=2&int2=9", 200, `["1","fizz","3","fizz","5","fizz","7","fizz","buzz","fizz","11","fizz","13","fizz","15","fizz","17","fizzbuzz","19","fizz","21","fizz","23","fizz","25","fizz","buzz","fizz","29","fizz"]`},
		{"limit=1000000", 400, `{"error":{"slug":"bad_parameters","detail":"Among available parameters (int1, int2 and limit) some were not between 0 and 65535"}}`},
		{"limit=a", 400, `{"error":{"slug":"bad_parameters","detail":"Among available parameters (int1, int2 and limit) some were not between 0 and 65535"}}`},
	}

	for _, tt := range contexts {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/fizzbuzz?"+tt.QueryString, nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, tt.HTTPCode, w.Code)
		assert.Equal(t, tt.JSON, w.Body.String())
	}
}
