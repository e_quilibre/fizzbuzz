package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFizzBuzz(t *testing.T) {
	tests := []struct {
		string1 string
		string2 string
		int1    int
		int2    int
		limit   int
		result  []string
	}{
		{"fizz", "buzz", 3, 5, 15,
			[]string{"1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizzbuzz"}},
		{"bat", "man", 2, 3, 9,
			[]string{"1", "bat", "man", "bat", "5", "batman", "7", "bat", "man"}},
		{"", "", 1, 1, 10,
			[]string{"", "", "", "", "", "", "", "", "", ""}},
	}

	for _, test := range tests {
		fizzbuzz := FizzBuzz(test.string1, test.string2, test.int1, test.int2, test.limit)
		assert.Equal(t, test.result, fizzbuzz)
	}
}
