package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	router := gin.Default()

	// Health request
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// FizzBuzz
	router.GET("/fizzbuzz", func(context *gin.Context) {
		string1 := context.DefaultQuery("string1", "fizz")
		string2 := context.DefaultQuery("string2", "buzz")
		int1, err1 := strconv.ParseUint(context.DefaultQuery("int1", "3"), 10, 16)
		int2, err2 := strconv.ParseUint(context.DefaultQuery("int2", "5"), 10, 16)
		limit, err3 := strconv.ParseUint(context.DefaultQuery("limit", "100"), 10, 16)

		if err1 != nil || err2 != nil || err3 != nil {
			context.String(http.StatusBadRequest, `{"error":{"slug":"bad_parameters","detail":"Among available parameters (int1, int2 and limit) some were not between 0 and 65535"}}`)
		} else {
			content := FizzBuzz(string1, string2, int(int1), int(int2), int(limit))
			context.JSON(http.StatusOK, content)
		}
	})

	return router
}

func main() {
	router := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	router.Run(":8080")
}
