### FizzBuzz REST API

build with GO version 1.11.2

#### How to run

I used govendor to install dependencies then, 

```bash
go test
go build 
./fizzbuzz
```

#### How to use 

_Basic usage_
```bash
curl -X GET 'http://localhost:8080/fizzbuzz'
```

_Limit customazition_
```bash
curl -X GET 'http://localhost:8080/fizzbuzz?limit=15'
```

_Values customazition_
```bash
curl -X GET 'http://localhost:8080/fizzbuzz?int1=2&int2=9'
```

_String customization_
```bash
curl -X GET 'http://localhost:8080/fizzbuzz?string1=bat&string2=man'
```

_Complete customization_
```bash
curl -X GET 'http://localhost:8080/fizzbuzz?limit=150&int1=2&int2=9&string1=bat&string2=man'
```


#### Based upon
As a ruby developer I search a solution equivalent to sinatra framework in golang
https://gin-gonic.github.io/gin/